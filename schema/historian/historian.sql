CREATE TABLE `historian`.`tenants` (
  `tenant` varchar(40) NOT NULL PRIMARY KEY
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `historian`.`tf` (
  `hash` varchar(40) NOT NULL PRIMARY KEY,
  `tenant` varchar(40) NOT NULL,
  `tf` varchar(12) NOT NULL,
  `ts` text NOT NULL,
  `start` bigint unsigned NOT NULL,
  `end` bigint unsigned NOT NULL,
  `settings` text,
  FOREIGN KEY (`tenant`)
            REFERENCES `tenants` (`tenant`)
            ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `historian`.`locations` (
  `tenant` varchar(40) NOT NULL,
  `ts` varchar(36) NOT NULL,
  `htable` varchar(36) NOT NULL,
  `partition` varchar(36) NOT NULL,
  `start` bigint unsigned NOT NULL,
  `end` bigint unsigned DEFAULT NULL,
  `records` int(11) NOT NULL DEFAULT '0',
  FOREIGN KEY (`tenant`)
          REFERENCES `tenants` (`tenant`)
          ON DELETE CASCADE,
  PRIMARY KEY (`tenant`, `ts`, `htable`, `partition`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
