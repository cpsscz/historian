CREATE TABLE `staging`.`records` (
  `tenant` varchar(40) NOT NULL,
  `ts` varchar(36) NOT NULL,
  `t` bigint unsigned NOT NULL,
  `v` TEXT NOT NULL,
  PRIMARY KEY (`tenant`, `ts`, `t`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
