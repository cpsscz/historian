# Actualizacion del Esquema de Cassandra

https://academy.datastax.com/node/6085/resource/ds220-data-modeling

El Historian de Sorba es una solucion basada en Cassandra. En la actualidad
estamos presentando ciertos problemas de performance de la base de datos
que resultan en errores durante la lectura y baja capacidad de escrituras
concurrentes. Este trabajo trata acerca de la solucion de estos problemas
haciendo un desarrollo guiado por pruebas de rendimiento.

El entorno para las pruebas de rendimiento ha sido el siguiente:

    - cluster Datastax Enterprise 6 con 3 nodos cada uno con:
        - 6GB RAM
        - 8 CPUs
        - 200GB storage
    - datastax opscenter con:
        - 8GB RAM
        - 8 CPUs
        - 200GB storage
    - stress test runner con:
        - 8GB RAM
        - 2 CPUs
        - 100GB storage

Los datos de monitoreo del cluster principal se almacenan en un cluster de
cassandra de un solo nodo en el servidor opscenter. De esta forma se evita
la interferencia que pudiera causar la insercion de la informacion de monitoreo
en el cluster de pruebas.

Importante notar que el cluster actual del ambiente de produccion se ejecuta
en Datastax Enterprise 4.8. No hemos hecho las pruebas en esa version del
producto porque queremos realizarla en el ambiente en que deseamos basar la
nueva version de nuestro producto.

Lo primero ha sido probar el performance del schema empleado originalmente, 
usando el siguiente comando:

```bash
cassandra-stress user profile=./perf-testing/original.yml n=1000000 ops\(insert=3,read1=1\) cl=ONE -node xxx.xxx.xxx.xxx
```

Este comando simula un ambiente donde se realizan 3 lecturas por cada 
escritura. Esto no se ajusta 100% al rate escritura/lectura que tenemos en 
produccion pero refleja el caso de una base de datos donde se escribe mucho
mas de lo que se lee.

El perfil empleado para modelar el esquema de la base de datos actual es:

```markdown
keyspace: sorba_original_perftesting

keyspace_definition: |
  CREATE KEYSPACE sorba_original_perftesting WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': 3};

table: records

table_definition: |
  CREATE TABLE records (
    process_id timeuuid,
    partition_key int,
    collected_at timestamp,
    received_at timestamp,
    value text,
    PRIMARY KEY ((process_id, partition_key), collected_at, received_at)
  ) WITH CLUSTERING ORDER BY (collected_at DESC, received_at ASC)
    AND read_repair_chance = 0.0
    AND dclocal_read_repair_chance = 0.1
    AND gc_grace_seconds = 864000
    AND bloom_filter_fp_chance = 0.01
    AND caching = { 'keys' : 'ALL', 'rows_per_partition' : 'NONE' }
    AND comment = ''
    AND compaction = { 'class' : 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy' }
    AND compression = { 'sstable_compression' : 'org.apache.cassandra.io.compress.LZ4Compressor' }
    AND default_time_to_live = 0
    AND speculative_retry = '99.0PERCENTILE'
    AND min_index_interval = 128
    AND max_index_interval = 2048;

columnspec:
  - name: process_id
    population: fixed(500)
  - name: collected_at
    cluster: gaussian(1..14400,10)
  - name: value
    size: gaussian(50..50000,10)

queries:
  read1:
    cql: select * from records where process_id = ? and partition_key = ? and collected_at >= ? and collected_at <= ?
    fields: samerow

insert:
  partitions: fixed(1)
  select: fixed(1)/1000
  batchtype: UNLOGGED
```

En este caso la poblacion del campo `process_id` coincide con el numero de 
assets que tiene Sorba a dia de hoy (13/11/2018). El cluster de `partition_key` 
corresponde a la posibilidad de tener assets con datos que se extienden en el 
tiempo entre 1 mes y 3 a;nos. El cluster de `collected_at`
se corresponde con la posible distribucion de records en una particion. Con 
el maximo considerando que se inserta un record por segundo (14400=4*60*60)
en una particion que enmarca 4 horas de datos (condiciones actuales). En el
caso de las dimensiones del campo `value` se han estimado tomando como maximo
un record que incluye el valor de 1000 tags donde cada valor ocupa 50 
caracteres (36 caracteres aproximadamente se usan para representar el id del
tag y lo restante para el valor y los demas elementos del JSON).

    En la actualidad tenemos assets que almacenan datos con frecuencia superior
    a 2.5 veces por segundo. En estos casos el performance de lectura y 
    escritura es mucho mas pobre que lo que se modela en esta prueba.

Despues de este test uno de los 3 nodos quedo en un estado inestable y por 
tanto dejo de atender solicitudes de clientes.

Para intentar mejorar el rendimiento del esquema de datos de Cassandra se
van a intentar los siguientes cambios:

  - des-agregar los datos de forma tal que los records en Cassandra almacenen
      el valor de un tag y no de todos los tags de un asset. Este simple
      cambio elimina 36bytes por cada tag en cada record de la db. Por ejemplo
      si tenemos un asset de 100 tags y una frecuencia de insercion de 1 record
      por segundo este cambio implica que se ahorran aprox. 13MB de 
      almacenamiento por hora para este asset.
  - el cambio descrito arriba elimina la necesidad de tener dos timestamps
      pues no es necesario particionar los datos de un timestamp. Ademas al
      eliminar el timestamp de recibido se evita la duplicacion de la 
      informacion cuando existe un error en el canal de recepcion y el emisor
      reintenta. De aqui que se propone eliminar el timestamp del momento en
      que se recibe la informacion en el historian.
  - unificar la llave de particion en un unico uuid. De esta forma se puede
      economizar la mitad del espacio dedicado a las cabeceras de las 
      particiones. Este no es un cambio tan necesario. Pero dejar el id del 
      asset/tag en el record tampoco va a permitir construir una consulta
      que abarque varias particiones.
      
Con estos cambios en mente empleados para modelar el esquema de la base de datos
 es:
 
 ```markdown
 keyspace: proposed_perftesting
 
 keyspace_definition: |
   CREATE KEYSPACE proposed_perftesting WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': 2};
 
 table: records
 
 table_definition: |
   CREATE TABLE records (
     p uuid,
     t timestamp,
     v text,
     PRIMARY KEY ((p), t)
   ) WITH CLUSTERING ORDER BY (t ASC)
     AND compression = { 'enabled' : false }
     AND compaction = { 'class' : 'org.apache.cassandra.db.compaction.TimeWindowCompactionStrategy' };
 
 columnspec:
   - name: p
     population: fixed(500000)
   - name: t
     cluster: uniform(1000..1000000)
   - name: v
     size: uniform(10..50)
 
 queries:
   read-range:
     cql: select * from records where p = ? and t >= ? and t <= ? limit 1000
     fields: samerow
   read-start:
     cql: select * from records where p = ? limit 1000
     fields: samerow
 
 insert:
   partitions: fixed(1)
   select: fixed(1)/100
   batchtype: UNLOGGED
 ```
 
 Para probar el performance del schema propuesto se ha usado el siguiente 
 comando:
 
 ```bash
 cassandra-stress user profile=./perf-testing/proposed.yml n=1000000 ops\(insert=3,read-range=1,read-start=1\) cl=ONE -node xxx.xxx.xxx.xxx
 ```
 
Este cambio en el schema de la base de datos no soluciona todos los problemas 
que esta presentando el producto pero ofrece caracteristicas que hacen posible
otra serie de cambios para solucionar otros problemas:

    - calculo de la cantidad optima de records por particion. Esto es posible 
      al poder calcular el tama;no promedio en el tiempo de los datos para un 
      tag. Cosa que no era posible para un asset por la variabilidad que 
      introduce el tener muchos tags con frecuencia de recoleccion diferentes.
        