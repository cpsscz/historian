# Conceptos

## Serie Temporal
Una serie temporal es una secuencia de valores indexados en el tiempo. O sea
es un conjunto que utiliza los valores de tiempo como universo (dominio). En el
caso de este producto la resolucion temporal es de 1 milisegundo. Cada serie
temporal esta identificada univocamente.

    En la actualidad el identificador unico de cada serie temporal se 
    corresponde con el identificador unico de un TAG en Sorba.
    
## Funcion Temporal
Una funcion temporal es una operacion sobre un grupo de conjuntos utilizando 
los valores de tiempo como universo de los mismos. En el caso del Historian
de Sorbotics los conjuntos sobre los que operan las funciones temporales son
las Series de Tiempo.

    Las funciones temporales son el mecanismo por el cual es posible obtener
    un conjunto (serie de tiempo) que es la union (intercepcion, etc) de
    varios otros conjuntos (series de tiempo). Por tanto es la herramienta
    de consulta mas versatil del Historian de Sorbotics. 

Las Funciones Temporales implementadas por el Historian son:

  - union (default)
  - intercepcion
  - even
  - delta
  - cyclic-raw
  - cyclic
  
### Funcion Temporal Union  
La funcion temporal union toma N series de tiempo y produce una nueva serie 
de tiempo que contiene todos los elementos, que pertenecen al menos a uno de 
los N conjuntos de entrada.

### Funcion Temporal Intercepcion
La funcion temporal intercepcion toma N series de tiempo y produce una nueva
serie de tiempo que contiene solo los elementos que necesariamente pertenecen
a los N conjuntos de entrada. 