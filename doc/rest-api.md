# API REST del Historian de Procesos de Sorbotics

La API del Historian de Procesos de Sorbotics se ha implementado usando el
estilo de arquitectura REST.

    Note que la mayoria de los recursos (excepto los recursos terminales) 
    implementan un tipo de contenido con soporte de hipermedia y otro sin
    soporte de ello. En el desarrollo de nuevos clientes use el primero para
    garantizar que su cliente pueda seguir funcionando aun cuando el historian
    evolucione.

## Recursos

## Recurso Raiz
El recurso raiz de la API es el unico punto de entrada conocido a priori de la
API del Historian. Todos los demas recursos se acceden usando controles de
hipermedia.

Este recurso expone la version de la API que ejecuta el servidor.

| Metodo | Tipo de Contenido                      |
|--------|----------------------------------------|
| GET    | application/hal+json, application/json |

| Relacion | Descripcion |
|----------|-------------|
| /rels/has-tenants | Permite acceder a la [Coleccion de Tenants](#coleccion-de-tenants) alojados en el servidor |

## Tenant
Permite acceder a un tenant en específico.

| Metodo | Tipo de Contenido                      |
|--------|----------------------------------------|
| GET    | application/hal+json, application/json |

| Relacion en Tenant      | Descripcion |
|-------------------------|-------------|
| /rels/has-tf-collection  | Permite acceder a la [Coleccion de Funciones Temporales](#coleccion-de-funciones-temporales) implementadas por la API para este tenant |
| /rels/has-ts-collection | Permite acceder a la [Coleccion de Series de Tiempo](#coleccion-de-series-de-tiempo) de este tenant alojadas en el servidor |

## Collection de Tenants
La coleccion de tenants lista todos los tenants alojados en el servidor. Usando
el metodo `POST` sobre esta coleccion es posible registrar nuevos tenants.

| Metodo | Tipo de Contenido                      |
|--------|----------------------------------------|
| GET    | application/hal+json, application/json |
| POST   | application/json                       |

| Relacion en Tenant      | Descripcion |
|-------------------------|-------------|
| /rels/has-tf-collection  | Permite acceder a la [Coleccion de Funciones Temporales](#coleccion-de-funciones-temporales) implementadas por la API para este tenant |
| /rels/has-ts-collection | Permite acceder a la [Coleccion de Series de Tiempo](#coleccion-de-series-de-tiempo) de este tenant alojadas en el servidor |

### Coleccion de Series de Tiempo
La coleccion de series de tiempo permite conocer que series de tiempo se 
encuentran alojadas en el servidor.

| Metodo | Tipo de Contenido    |
|--------|----------------------|
| GET    | application/hal+json |

| Relacion en Serie de Tiempo | Descripcion |
|-------------------|-------------|
| /rels/ts-has-data | Permite acceder a la [Coleccion de Datos de una Serie de Tiempo](#coleccion-de-datos-de-serie-de-tiempo) |

### Coleccion de Datos de Serie de Tiempo
Este recurso es la API mas simple para recuperar o modificar los puntos de 
datos que componen una serie de tiempo.

Opcionalmente es posible parametrizar la solicitud usando los parametros 
`start`, `end` y `limit`. Al usarlos es posible solicitar un subconjunto del
conjunto objeto limitandolo en su universo. Usando esta parametrizacion es
posible implementar paginacion por ventana corrediza.

| Metodo | Tipo de Contenido  |
|--------|--------------------|
| GET    | application/json   |
| POST   | application/json   |

| Parametro Query | Descripcion |
|-----------------|-------------|
| start (numero)  | el timestamp de inicio de los datos (UNIX timestamp presicion de milisegundos) |
| end (numero)    | el timestamp de fin de los datos (UNIX timestamp presicion de milisegundos) |
| limit (numero)  | cantidad maxima de puntos de datos a recuperar |

### Coleccion de Funciones Temporales
La coleccion de funciones temporales permite conocer que funciones temporales
se encuentran alojadas en este servidor. Ademas usando el metodo `POST` sobre
esta coleccion es posible registrar nuevas funciones temporales.

Debe notarse que lo que almacena esta coleccion no es la implementacion 
algorithmica de una funcion temporal sino la definicion de la aplicacion de las
mismas. O sea cada entrada de esta coleccion se conforma por la referencia a 
una funcion temporal, los conjuntos de entrada (identificados por su 
identificador unico) asi como el alcance sobre los mismos (inicio y fin en el 
dominio del tiempo), los parametros especificos de ajuste del algorithmo y
la direccion del conjunto resultado. 

| Metodo | Tipo de Contenido                      |
|--------|----------------------------------------|
| GET    | application/hal+json, application/json |
| POST   | application/json                       |

| Relacion en Funcion Temporal | Descripcion |
|-------------------|-------------|
| self              | Permite acceder a la [Funcion Temporal](#funcion-temporal) |
| /rels/tf-has-data | Permite acceder a la [Coleccion de Datos de una Funcion Temporal](#coleccion-de-datos-de-una-funcion-temporal) |

### Funcion Temporal
La funcion temporal permite conocer los detalles de la misma.

Debe notarse que lo que almacena esta coleccion no es la implementacion 
algorithmica de una funcion temporal sino la definicion de la aplicacion de las
mismas. O sea cada entrada de esta coleccion se conforma por la referencia a 
una funcion temporal, los conjuntos de entrada (identificados por su 
identificador unico) asi como el alcance sobre los mismos (inicio y fin en el 
dominio del tiempo), los parametros especificos de ajuste del algorithmo y
la direccion del conjunto resultado. 

| Metodo | Tipo de Contenido                      |
|--------|----------------------------------------|
| GET    | application/hal+json, application/json |

| Relacion | Descripcion |
|-------------------|-------------|
| /rels/tf-has-data | Permite acceder a la [Coleccion de Datos de una Funcion Temporal](#coleccion-de-datos-de-una-funcion-temporal) |


### Coleccion de Datos de una Funcion Temporal
Este endpoint permite recuperar el conjunto resultado de aplicar una funcion
temporal en especifico sobre un grupo de conjuntos de entrada.

Opcionalmente es posible parametrizar la solicitud usando los parametros 
`start`, `end` y `limit`. Al usarlos es posible solicitar un subconjunto del
conjunto resultado limitandolo en su universo. Usando esta parametrizacion es
posible implementar paginacion por ventana corrediza.

| Metodo | Tipo de Contenido  |
|--------|--------------------|
| GET    | application/json   |

| Parametro Query | Descripcion |
|-----------------|-------------|
| start (numero)  | el timestamp de inicio de los datos (UNIX timestamp presicion de milisegundos) |
| end (numero)    | el timestamp de fin de los datos (UNIX timestamp presicion de milisegundos) |
| limit (numero)  | cantidad maxima de puntos de datos a recuperar |