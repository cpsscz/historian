/* test suite for the package entry point */

"use strict";

const test = require("ava");

const index = require(".");

test("package export Application symbol", t => t.truthy(index.Application));
