// @flow

"use strict";

/*::
const express = require("express");

import { Historian } from "../historian";
*/

const controllers = require("./controllers");

/** Setup `express` router using controllers
 *
 * @param router `express` router
 * @param historian Historian
 * @returns {express.Router} `express` router
 */
function setupRouter(
  router /*: express.Router */,
  historian /*: Historian */
) /*: express.Router */ {
  new controllers.RootController().setUpRoute(router.route("/"));

  new controllers.TenantsController(historian.final.mysql).setUpRoute(
    router.route("/tenants")
  );

  new controllers.TenantController(historian.final.mysql).setUpRoute(
    router.route("/tenants/:tenant")
  );

  new controllers.TemporalFunctionCollectionController(
    historian.final.mysq
  ).setUpRoute(router.route("/tenants/:tenant/tf"));

  new controllers.TemporalFunctionController(historian.final.mysql).setUpRoute(
    router.route("/tenants/:tenant/tf/:hash")
  );

  new controllers.TemporalFunctionDataController(historian).setUpRoute(
    router.route("/tenants/:tenant/tf/:hash/data")
  );

  new controllers.TimeSeriesCollectionController(historian).setUpRoute(
    router.route("/tenants/:tenant/ts")
  );

  new controllers.TimeSeriesController(historian).setUpRoute(
    router.route("/tenants/:tenant/ts/:tsId")
  );

  new controllers.TimeSeriesDataController(historian).setUpRoute(
    router.route("/tenants/:tenant/ts/:ts/data")
  );

  return router;
}

module.exports = { setupRouter };
