/* test suite for the web package */

"use strict";

const test = require("ava");

const web = require(".");

test("package export setupRouter symbol", t => t.truthy(web.setupRouter));
