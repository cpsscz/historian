// @flow

"use strict";

const { setupRouter } = require("./setup-router");

module.exports = {
  setupRouter
};
