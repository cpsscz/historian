// @flow

"use strict";

/*::
const express = require("express");

import { Historian } from "../../historian";
import { Controller } from "./controller";
*/

const JSONStream = require("JSONStream");
const pump = require("pump");

const connectionKeeper = require("../../common/connection-keeper");
const limitStream = require("../../common/limit-stream");

/** Time Series Data Web controller
 *
 * Implement Time Series data retrieval and update over the Web.
 */
class TimeSeriesDataController /*:: implements Controller */ {
  /*::
  historian: Historian;
  */

  constructor(historian /*: Historian */) {
    this.historian = historian;
  }

  /** Setup an `express` route for this controller
   *
   * @param route `express` route
   */
  setUpRoute(route /*: express.Route */) {
    route.get(this.get.bind(this));
    route.post(this.post.bind(this));
  }

  get(
    req /*: express.Request */,
    res /*: express.Response */,
    next /*: express.NextFunction */
  ) {
    const tenant = req.params.tenant;
    const ts = req.params.ts;
    const start = req.query.start ? parseInt(req.query.start) : 0;
    const end = req.query.end ? parseInt(req.query.end) : Infinity;
    const reqStart = Date.now();

    res.setHeader("Connection", "Transfer-Encoding");
    res.setHeader("Transfer-Encoding", "chunked");

    this.historian.query(tenant, ts, start, end).then(
      readable => {
        const streamRetrieved = Date.now();

        const pipeline = [readable];

        // apply response limit if instruction included in query
        if (req.query.limit) {
          pipeline.push(
            new limitStream.LimitSteam(
              pipeline[pipeline.length - 1],
              parseInt(req.query.limit, 10)
            )
          );
        }

        pipeline.push(new connectionKeeper.ConnectionKeeper());

        pipeline.push(
          JSONStream.stringify(
            `{ 
          "req-start": ${reqStart}, 
          "stream-retrieved": ${streamRetrieved}, 
          "_embedded": { "ts-data": [`,
            ",",
            "]}}"
          )
        );

        pipeline.push(res);

        res.format({
          "application/hal+json": () =>
            pump(...pipeline, err => {
              if (err && err.message !== "premature close") {
                next(err);
              }
            }),
          json: () =>
            pump(...pipeline, err => {
              if (err && err.message !== "premature close") {
                next(err);
              }
            }),
          default: () => res.status(406).end()
        });
      },
      err => next(err)
    );
  }

  post(
    req /*: express.Request */,
    res /*: express.Response */,
    next /*: express.NextFunction */
  ) {
    // TODO: missing input validation

    const tenant = req.params.tenant;
    const ts = req.params.ts;

    const destination = JSONStream.parse("data.*");

    pump(req, destination);

    this.historian
      .insert(tenant, ts, destination)
      .then(() => res.end(), err => next(err));
  }
}

module.exports = { TimeSeriesDataController };
