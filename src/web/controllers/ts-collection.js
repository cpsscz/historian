// @flow

"use strict";

const stream = require("stream");
const url = require("url");

/*::
const express = require("express");

import { Historian } from "../../historian";
import { Controller } from "./controller";
*/

const JSONStream = require("JSONStream");
const pump = require("pump");

class SetSelfLinkTransform extends stream.Transform {
  constructor(req) {
    super({ objectMode: true, allowHalfOpen: false });

    this.req = req;
  }

  _transform(item, _, cb) {
    item._links = {
      "/rels/ts-has-data": {
        href: url.resolve(this.req.originalUrl, `./ts/${item.ts}/data`)
      },
      self: {
        href: url.resolve(this.req.originalUrl, `./ts/${item.ts}`)
      }
    };

    cb(null, item);
  }
}

/** Time Series collection Web controller
 *
 * Implement Time Series collection retrieval over the Web.
 */
class TimeSeriesCollectionController /*:: implements Controller */ {
  /*::
  historian: Historian;
  */

  constructor(historian /*: Historian */) {
    this.historian = historian;
  }

  /** Setup an `express` route for this controller
   *
   * @param route `express` route
   */
  setUpRoute(route /*: express.Route */) {
    route.get(this.get.bind(this));
  }

  get(
    req /*: express.Request */,
    res /*: express.Response */,
    next /*: express.NextFunction */
  ) {
    res.setHeader("Connection", "Transfer-Encoding");
    res.setHeader("Transfer-Encoding", "chunked");

    this.historian.list(req.params.tenant).then(
      readable => {
        const pipeline = [
          readable,
          JSONStream.stringify('{ "_embedded": { "ts": [', ",", "]}}"),
          res
        ];

        res.format({
          "application/hal+json": () => {
            pipeline.splice(1, 0, new SetSelfLinkTransform(req));

            pump(...pipeline, err => {
              if (err) {
                next(err);
              }
            });
          },
          json: () =>
            pump(...pipeline, err => {
              if (err) {
                next(err);
              }
            }),
          default: () => res.status(406).end()
        });
      },
      err => next(err)
    );
  }
}

module.exports = { TimeSeriesCollectionController };
