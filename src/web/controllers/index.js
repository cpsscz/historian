// @flow

"use strict";

const root = require("./root");
const tenant = require("./tenant");
const tenants = require("./tenants");
const tfData = require("./tf-data");
const tf = require("./tf");
const tfCollection = require("./tf-collection");
const tsData = require("./ts-data");
const tsCollection = require("./ts-collection");
const ts = require("./ts");

module.exports = {
  RootController: root.RootController,
  TemporalFunctionDataController: tfData.TemporalFunctionDataController,
  TemporalFunctionController: tf.TemporalFunctionController,
  TemporalFunctionCollectionController:
    tfCollection.TemporalFunctionCollectionController,
  TenantController: tenant.TenantController,
  TenantsController: tenants.TenantsController,
  TimeSeriesDataController: tsData.TimeSeriesDataController,
  TimeSeriesCollectionController: tsCollection.TimeSeriesCollectionController,
  TimeSeriesController: ts.TimeSeriesController
};
