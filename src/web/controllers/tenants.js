// @flow

"use strict";

const stream = require("stream");
const url = require("url");

/*::
const express = require("express");

import { Controller } from "./controller";
*/

const bodyParser = require("body-parser");
const JSONStream = require("JSONStream");
const pump = require("pump");

class SetSelfLinkTransform extends stream.Transform {
  constructor(req) {
    super({ objectMode: true, allowHalfOpen: false });

    this.req = req;
  }

  _transform(item, _, cb) {
    item._links = {
      "/rels/has-tf-collection": {
        href: url.resolve(this.req.originalUrl, `./tenants/${item.tenant}/tf`)
      },
      "/rels/has-ts-collection": {
        href: url.resolve(this.req.originalUrl, `./tenants/${item.tenant}/ts`)
      }
    };

    cb(null, item);
  }
}

class TenantsController /*:: implements Controller */ {
  constructor(mysql) {
    this.mysql = mysql;
  }

  /** Setup an `express` route for this controller
   *
   * @param route `express` route
   */
  setUpRoute(route /*: express.Route */) {
    route.get(this.get.bind(this));
    route.post(bodyParser.json(), this.post.bind(this));
  }

  get(
    req /*: express.Request */,
    res /*: express.Response */,
    next /*: express.NextFunction */
  ) {
    res.setHeader("Connection", "Transfer-Encoding");
    res.setHeader("Transfer-Encoding", "chunked");

    const pipeline = [
      this.mysql.query("SELECT * FROM `tenants`").stream(),
      JSONStream.stringify('{ "_embedded": { "tenants": [', ",", "]}}"),
      res
    ];

    res.format({
      "application/hal+json": () => {
        pipeline.splice(1, 0, new SetSelfLinkTransform(req));

        pump(...pipeline, err => {
          if (err) {
            next(err);
          }
        });
      },
      json: () =>
        pump(...pipeline, err => {
          if (err) {
            next(err);
          }
        }),
      default: () => res.status(406).end()
    });
  }

  post(
    req /*: express.Request */,
    res /*: express.Response */,
    next /*: express.NextFunction */
  ) {
    // TODO: missing input validation

    let { tenant } = req.body;

    this.mysql
      .promise()
      .query("INSERT IGNORE INTO `tenants` (`tenant`) VALUES (?)", [tenant])
      .then(
        result => {
          const status = result.affectedRows > 0 ? 201 : 200;

          res
            .status(status)
            .location(url.resolve(req.originalUrl, `./tenants/${tenant}`))
            .end();
        },
        err => next(err)
      );
  }
}

module.exports = { TenantsController };
