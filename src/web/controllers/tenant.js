// @flow

"use strict";

const url = require("url");

/*::
const express = require("express");

import { Controller } from "./controller";
*/

class TenantController /*:: implements Controller */ {
  constructor(mysql) {
    this.mysql = mysql;
  }

  /** Setup an `express` route for this controller
   *
   * @param route `express` route
   */
  setUpRoute(route /*: express.Route */) {
    route.get(this.get.bind(this));
  }

  async get(
    req /*: express.Request */,
    res /*: express.Response */,
    next /*: express.NextFunction */
  ) {
    try {
      const [rows] = await this.mysql
        .promise()
        .execute("SELECT * FROM `tenants` WHERE tenant = ?", [
          req.params.tenant
        ]);

      res.format({
        "application/hal+json": () => {
          res.json({
            ...rows[0],
            _links: {
              "/rels/has-tf-collection": {
                href: url.resolve(req.originalUrl, `./${rows[0].tenant}/tf`)
              },
              "/rels/has-ts-collection": {
                href: url.resolve(req.originalUrl, `./${rows[0].tenant}/ts`)
              }
            }
          });
        },
        json: () => res.json(rows[0]),
        default: () => res.status(406).end()
      });
    } catch (e) {
      next(e);
    }
  }
}

module.exports = { TenantController };
