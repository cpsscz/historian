// @flow

"use strict";

const stream = require("stream");
const url = require("url");
const Ajv = require("ajv");
const ajv = new Ajv({ allErrors: true });
/*::
const express = require("express");

import { Controller } from "./controller";
*/

const bodyParser = require("body-parser");
const JSONStream = require("JSONStream");
const objectHash = require("object-hash");
const pump = require("pump");
const schema = require("../../common/tf-collection.schema.json");

class SetSelfLinkTransform extends stream.Transform {
  constructor(req) {
    super({ objectMode: true, allowHalfOpen: false });

    this.req = req;
  }

  _transform(item, _, cb) {
    item._links = {
      self: { href: url.resolve(this.req.originalUrl, `./tf/${item.hash}`) },
      "/rels/tf-has-data": {
        href: url.resolve(this.req.originalUrl, `./tf/${item.hash}/data`)
      }
    };

    cb(null, item);
  }
}

class TemporalFunctionCollectionController /*: implements Controller */ {
  constructor(mysql) {
    this.mysql = mysql;
    this.validate = ajv.compile(schema);
  }

  /** Setup an `express` route for this controller
   *
   * @param route `express` route
   */
  setUpRoute(route /*: express.Route */) {
    route.get(this.get.bind(this));
    route.post(bodyParser.json(), this.post.bind(this));
  }

  get(
    req /*: express.Request */,
    res /*: express.Response */,
    next /*: express.NextFunction */
  ) {
    res.setHeader("Connection", "Transfer-Encoding");
    res.setHeader("Transfer-Encoding", "chunked");

    const pipeline = [
      this.mysql
        .execute("SELECT * FROM `tf` WHERE tenant = ?", [req.params.tenant])
        .stream(),
      JSONStream.stringify('{ "_embedded": { "tf": [', ",", "]}}"),
      res
    ];

    res.format({
      "application/hal+json": () => {
        pipeline.splice(1, 0, new SetSelfLinkTransform(req));

        pump(...pipeline, err => {
          if (err) {
            next(err);
          }
        });
      },
      json: () =>
        pump(...pipeline, err => {
          if (err) {
            next(err);
          }
        }),
      default: () => res.status(406).end()
    });
  }

  post(
    req /*: express.Request */,
    res /*: express.Response */,
    next /*: express.NextFunction */
  ) {
    // TODO: missing input validation

    let { tenant } = req.params;
    let { tf, ts, start, end, settings } = req.body;
    const reqBody = req.body;

    if (!tf) {
      tf = "union";
    }

    let valid = this.validate(reqBody);
    if (!valid) {
      return next(new Error("Invalid JSON schema.", 400));
    }

    const hash = objectHash({ tenant, tf, ts, start, end, settings });

    this.mysql
      .promise()
      .execute(
        "INSERT IGNORE INTO `tf` (`hash`, `tenant`, `tf`, `ts`, `start`, `end`, `settings`) VALUES (?, ?, ?, ?, ?, ?, ?)",
        [hash, tenant, tf, ts, start, end, settings]
      )
      .then(
        result => {
          const status = result.affectedRows > 0 ? 201 : 200;

          res
            .status(status)
            .location(url.resolve(req.originalUrl, `./tf/${hash}/data`))
            .end();
        },
        err => next(err)
      );
  }
}

module.exports = { TemporalFunctionCollectionController };
