/* test suite for the web/controller package */

"use strict";

const test = require("ava");

const controllers = require(".");

test("package export RootController symbol", t =>
  t.truthy(controllers.RootController));

test("package export TemporalFunctionController symbol", t =>
  t.truthy(controllers.TemporalFunctionController));

test("package export TemporalFunctionDataController symbol", t =>
  t.truthy(controllers.TemporalFunctionDataController));

test("package export TemporalFunctionCollectionController symbol", t =>
  t.truthy(controllers.TemporalFunctionCollectionController));

test("package export TenantController symbol", t =>
  t.truthy(controllers.TenantController));

test("package export TenantsController symbol", t =>
  t.truthy(controllers.TenantsController));

test("package export TimeSeriesCollectionController symbol", t =>
  t.truthy(controllers.TimeSeriesCollectionController));

test("package export TimeSeriesDataController symbol", t =>
  t.truthy(controllers.TimeSeriesDataController));

test("package export TimeSeriesController symbol", t =>
  t.truthy(controllers.TimeSeriesController));
