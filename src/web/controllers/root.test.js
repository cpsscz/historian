/* test suite for the web controller for the root resource of the API */

"use strict";

const express = require("express");
const supertest = require("supertest");
const test = require("ava");

const { RootController } = require("./root");

let listener;

test.before(() => {
  listener = express();

  new RootController().setUpRoute(listener.route("/"));
});

test("handle GET method", async t => {
  const res = await supertest(listener).get("/");

  t.is(res.status, 200);
});

test("default media type is application/hal+json", async t => {
  const res = await supertest(listener).get("/");

  t.is(res.type, "application/hal+json");
});

test("provide application/json", async t => {
  const res = await supertest(listener)
    .get("/")
    .set("Accept", "application/json");

  t.is(res.type, "application/json");
});

test("response include server version", async t => {
  const res = await supertest(listener).get("/");

  t.truthy(res.body.version);
});

test("response include /rels/has-tenants relation", async t => {
  const res = await supertest(listener).get("/");

  t.truthy(res.body._links["/rels/has-tenants"].href);
});

test("implement server side content negotiation", async t => {
  const res = await supertest(listener)
    .get("/")
    .set("Accept", "no-acceptable-media-type");

  t.is(res.status, 406);
});
