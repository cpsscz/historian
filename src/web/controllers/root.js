// @flow

"use strict";

/*::
const express = require("express");

import { Controller } from "./controller";
*/

const url = require("url");

/** Web controller for the root resource */
class RootController /*:: implements Controller */ {
  /** Root resource */
  static get resource() {
    return { version: "1.0.0" };
  }

  /** Setup an `express` route for this controller
   *
   * @param route `express` route
   */
  setUpRoute(route /*: express.Route */) {
    route.get(this.get.bind(this));
  }

  /** Handle web requests with GET method
   *
   * This handler servers the root resource of the api that provide the
   * relations used to access API resources.
   *
   * @param req Web request
   * @param res Web response
   */
  get(req /*: express.Request */, res /*: express.Response */) {
    res.format({
      "application/hal+json": () => {
        res.write(
          JSON.stringify({
            ...RootController.resource,
            _links: {
              self: { href: req.originalUrl },
              "/rels/has-tenants": {
                href: url.resolve(req.originalUrl, "./tenants")
              }
            }
          })
        );
        res.end();
      },
      json: () => res.json(RootController.resource),
      default: () => res.status(406).end()
    });
  }
}

module.exports = { RootController };
