// @flow

"use strict";

const stream = require("stream");

/*::
const express = require("express");

import { Historian } from "../../historian";
import { Controller } from "./controller";
*/

const JSONStream = require("JSONStream");
const pump = require("pump");
const streamCombine = require("stream-combine");

const connectionKeeper = require("../../common/connection-keeper");
const limitStream = require("../../common/limit-stream");
const tfFactories = require("../../common/tf");

class AggregateTransform extends stream.Transform {
  //private size: number;

  constructor(size) {
    super({ objectMode: true, allowHalfOpen: false });

    this.size = size;
  }

  _transform(item, _, cb) {
    let record = {
      t: item.t,
      v: new Array(this.size).fill(null)
    };

    item.data.forEach((r, i) => {
      record.v[item.indexes[i]] = r.v;
    });

    cb(null, record);
  }
}

/** Temporal Function Web controller */
class TemporalFunctionDataController /*:: implements Controller */ {
  /*::
  historian: Historian;
  */

  constructor(historian /*: Historian */) {
    this.historian = historian;
  }

  /** Setup an `express` route for this controller
   *
   * @param route `express` route
   */
  setUpRoute(route /*: express.Route */) {
    route.get(this.get.bind(this));
  }

  async get(
    req /*: express.Request */,
    res /*: express.Response */,
    next /*: express.NextFunction */
  ) {
    const tenant = req.params.tenant;
    const hash = req.params.hash;
    const reqStart = Date.now();

    res.setHeader("Connection", "Transfer-Encoding");
    res.setHeader("Content-Type", "application/json; charset=utf-8");
    res.setHeader("Transfer-Encoding", "chunked");

    let [
      [{ tf, ts, start, end, settings }]
    ] = await this.historian.final.mysql
      .promise()
      .execute("SELECT * FROM `tf` WHERE `tenant` = ? AND `hash` = ?", [
        tenant,
        hash
      ]);

    const tfRetrieved = Date.now();

    ts = ts.split(",");

    if (req.query.start) {
      const queryStart = parseInt(req.query.start, 10);

      if (queryStart > start && queryStart < end) {
        start = queryStart;
      }
    }

    if (req.query.end) {
      const queryEnd = parseInt(req.query.end, 10);

      if (queryEnd > start && queryEnd < end) {
        end = queryEnd;
      }
    }

    Promise.all(
      ts.map(ts => this.historian.query(tenant, ts, start, end))
    ).then(
      readableStreams => {
        const streamRetrieved = Date.now();

        const pipeline = [
          new streamCombine(readableStreams, "t"),
          new AggregateTransform(ts.length)
        ];

        // apply temporal function if not union
        if (tf !== "union") {
          pipeline.push(
            new tfFactories[tf](
              ts,
              start,
              end,
              settings ? JSON.parse(settings) : undefined
            )
          );
        }

        // apply response limit if instruction included in query
        if (req.query.limit) {
          pipeline.push(
            new limitStream.LimitSteam(
              pipeline[pipeline.length - 1],
              parseInt(req.query.limit, 10)
            )
          );
        }

        // ensure connection with the client is keep open
        pipeline.push(new connectionKeeper.ConnectionKeeper());

        // transform data stream into text JSON
        pipeline.push(
          JSONStream.stringify(
            `{ "tf": "${tf}",
          "ts": ${JSON.stringify(ts)},
          "start": ${start},
          "end": ${end},
          "settings": ${JSON.stringify(settings)},
          "req-start": ${reqStart},
          "tf-retrieved": ${tfRetrieved},
          "stream-retrieved": ${streamRetrieved},
          "_embedded": { "tf-data": [`,
            ",",
            "]}}"
          )
        );

        // pipe response to output
        pipeline.push(res);

        pump(...pipeline, err => {
          if (
            err &&
            err.message !== "premature close" &&
            err.message !== "write after end"
          ) {
            next(err);
          }
        });
      },
      err => next(err)
    );
  }
}

module.exports = { TemporalFunctionDataController };
