/* @flow */
/*:: 
  import type {Controller} from './controller';
  import type {Historian} from '../../historian';
  import type {Route,Request,Response,NextFunction} from  'express';
*/
"use strict";

const pump = require("pump");
const stream = require("stream");
const url = require("url");

class FilterTsTransform extends stream.Transform {
  /*:: 
    tsId: string
  */
  constructor(tsId) {
    super({ objectMode: true, allowHalfOpen: false });
    this.tsId = tsId;
  }

  _transform(item /*: any */, _, cb) {
    if (item.ts === this.tsId) {
      this.push(item);
    }
    cb();
  }
}

class SetLinksTransform extends stream.Transform {
  /*:: 
    req: Request
  */
  constructor(req /*: Request */) {
    super({ objectMode: true, allowHalfOpen: false });
    this.req = req;
  }
  _transform(item, _, cb) {
    const { tsId } = this.req.params;
    item._links = {
      self: {
        href: this.req.originalUrl
      },
      "/rels/ts-has-data": {
        href: url.resolve(this.req.originalUrl, `./ts/${tsId}/data`)
      }
    };
    cb(null, JSON.stringify(item));
  }
}

class TimeSeriesController /*:: implements Controller */ {
  /*::
    historian: Historian
  */
  constructor(historian /*: Historian */) {
    this.historian = historian;
  }
  setUpRoute(route /*: Route */) {
    route.get(this.get.bind(this));
  }
  get(req /*: Request */, res /*: Response */, next /*: NextFunction */) {
    const { tenant, tsId } = req.params;

    res.setHeader("Connection", "Transfer-Encoding");
    res.setHeader("Transfer-Encoding", "chunked");

    const filterTs = new FilterTsTransform(tsId);
    const setLinks = new SetLinksTransform(req);

    this.historian
      .list(tenant)
      .then(readable => {
        res.format({
          "application/hal+json": () => {
            pump(readable, filterTs, setLinks, res, err => {
              if (err) {
                next(err);
              }
            });
          },
          json: () => {
            pump(readable, filterTs, res, err => {
              if (err) {
                next(err);
              }
            });
          },
          default: () => res.status(406).end()
        });
      })
      .catch(err => next(err));
  }
}

module.exports = { TimeSeriesController };
