// @flow

"use strict";

const url = require("url");

/*::
const express = require("express");

import { Controller } from "./controller";
*/

class TemporalFunctionController /*: implements Controller */ {
  constructor(mysql) {
    this.mysql = mysql;
  }

  /** Setup an `express` route for this controller
   *
   * @param route `express` route
   */
  setUpRoute(route /*: express.Route */) {
    route.get(this.get.bind(this));
  }

  async get(
    req /*: express.Request */,
    res /*: express.Response */,
    next /*: express.NextFunction */
  ) {
    try {
      const [rows] = await this.mysql
        .promise()
        .execute("SELECT * FROM `tf` WHERE tenant = ? AND hash = ?", [
          req.params.tenant,
          req.params.hash
        ]);

      res.format({
        "application/hal+json": () => {
          res.json({
            ...rows[0],
            _links: {
              self: {
                href: url.resolve(req.originalUrl, `./tf/${rows[0].hash}`)
              },
              "/rels/tf-has-data": {
                href: url.resolve(req.originalUrl, `./tf/${rows[0].hash}/data`)
              }
            }
          });
        },
        json: () => res.json(rows[0]),
        default: () => res.status(406).end()
      });
    } catch (e) {
      next(e);
    }
  }
}

module.exports = { TemporalFunctionController };
