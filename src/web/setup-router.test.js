/* test suite for the setupRouter function */

"use strict";

const express = require("express");
const test = require("ava");

const { setupRouter } = require("./setup-router");

test.skip("return the same router", t => {
  const router = express.Router();

  t.is(setupRouter(router), router);
});

test.skip("setup route for the root resource", t => {
  const router = express.Router();
  let routeRoot = false;
  router.route = path => {
    if (path === "/") {
      routeRoot = true;
    }

    return { get: () => null };
  };

  setupRouter(router);

  t.true(routeRoot);
});
