/* test suite for the application class */

"use strict";

const supertest = require("supertest");
const test = require("ava");

const { Application } = require("./application");

let application /*: Application */;

test.before(() => {
  application = new Application({
    listener: JSON.stringify({
      "some-setting": "some-value"
    })
  });
});

test.skip("application has `listener` attr", t =>
  t.truthy(application.listener));

test.skip("application `listener` attr is a function", t =>
  t.true(typeof application.listener === "function"));

test.skip("application `listener` attr handle Web request events", async t => {
  const res = await supertest(application.listener).get("/");

  t.is(res.status, 200);
});

test.skip("application set express settings using the listener setting", t => {
  t.is(application.listener.get("some-setting"), "some-value");
});
