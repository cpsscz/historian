// @flow

"use strict";

/*::
const stream = require("stream");

import { Record } from "./record";

// Interface into the Historian
export interface Historian {
  // List all unique identifiers of stored Time Series
  //
  // @param {String} tenant Tenant
  @returns {Promise<stream.Readable<any>>} Promise to return a readable stream
  list(tenant: string): Promise<stream.Readable>

  // Query a time series
  //
  // Query the Process Historian to retrieve all values of a specific time
  // series in a given range of time.
  //
  // @param {String} tenant Tenant
  // @param {String} ts Time Series unique identifier
  // @param {Number} start Start timestamp
  // @param {Number} end End Timestamp
  // @returns {Promise<stream.Readable<Record>>} Promise to return a readable stream
  query(tenant: string, ts: string, start: number, end: number): Promise<stream.Readable>;

  // Insert records into time series
  //
  // @param {String} tenant Tenant
  // @param {String} ts Time Series unique identifier
  // @param {stream.Readable<Record>}
  // @return {Promise<void>} Promise to return when data write ends
  insert(tenant: string, ts: string, readable: stream.Readable, writable?: stream.Writable): Promise<void>;

  // Delete all Time Series data within a range
  //
  // @param {String} tenant Tenant
  // @param {String} ts Time Series unique identifier
  // @param {Number} start Start timestamp
  // @param {Number} end End Timestamp
  // @return {Promise<void>} Promise to return when data deletion ends
  delete(tenant: string, ts: string, start: number, end: number): Promise<void>;
}
*/

