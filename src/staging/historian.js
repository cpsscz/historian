// @flow

"use strict";

const mysql = require("mysql2");
const pump = require("pump");

/*::
import { Historian as HistorianInterface } from "../historian";
*/

const setTenantTs = require("../common/set-tenant-ts");

const writer = require("./writer");

/** Staging Historian
 *
 * Store history in a staging area. Latter that history is moved into a
 * permanent storage media.
 */
class Historian /*:: implements HistorianInterface */ {
  static get DEFAULT_SETTINGS() {
    return {
      DATABASE_URL: "mysql://user:password@mysql/staging"
    };
  }

  /*::
  settings: any;
  */

  constructor(settings /*: any */ = {}) {
    this.settings = Object.assign({}, Historian.DEFAULT_SETTINGS, settings);
  }

  get mysql() {
    if (!this._mysql) {
      this._mysql = mysql.createPool(this.settings.DATABASE_URL);
    }

    return this._mysql;
  }

  async list(tenant /*: string */) {
    return this.mysql
      .query(
        "SELECT DISTINCT `ts` FROM `records` WHERE `tenant` = ? " +
          "ORDER BY `ts` ASC",
        [tenant]
      )
      .stream();
  }

  async query(tenant /*: string */, ts /*: string */, start, end) {
    if (end === Infinity) {
      end = Math.pow(2, 64) - 1;
    }

    return this.mysql
      .query(
        "SELECT `t`, `v` FROM `records` WHERE `tenant` = ? AND `ts` = ? " +
          "AND `t` >= ? AND `t` <= ? ORDER BY `t`",
        [tenant, ts, start, end]
      )
      .stream();
  }

  insert(tenant /*: string */, ts /*: string */, readable, writable) {
    return new Promise((resolve, reject) => {
      const pipeline = [
        readable,
        new setTenantTs.SetTenantTSTransform(tenant, ts)
      ];

      if (writable) {
        pipeline.push(new writer.WriterTransform(this.mysql));
        pipeline.push(writable);
      } else {
        pipeline.push(new writer.Writer(this.mysql));
      }

      pump(...pipeline, err => (err ? reject(err) : resolve()));
    });
  }

  async delete(
    tenant /*: string */,
    ts /*: string */,
    start /*: number */,
    end /*: number */
  ) {
    if (end === Infinity) {
      end = Math.pow(2, 64) - 1;
    }

    await this.mysql
      .promise()
      .query(
        "DELETE FROM `records` WHERE `tenant` = ? AND `ts` = ? " +
          "AND `t` >= ? AND `t` <= ?",
        [tenant, ts, start, end]
      );
  }
}

module.exports = { Historian };
