// @flow

"use strict";

const stream = require("stream");

/*::
const mysql = require("mysql2");
*/

class Writer extends stream.Writable {
  static get query() {
    return (
      "INSERT IGNORE INTO `records` (`tenant`, `ts`, `t`, `v`) " +
      "VALUES (?, ?, ?, ?)"
    );
  }

  /*::
  mysql: mysql.Client;
  */

  constructor(mysql) {
    super({ objectMode: true });

    this.mysql = mysql;
  }

  //$FlowFixMe
  _write({ tenant, ts, t, v }, _, done) {
    this.mysql.execute(Writer.query, [tenant, ts, t, v], err => done(err));
  }
}

class WriterTransform extends stream.Transform {
  /*::
  mysql: mysql.Client;
  */

  constructor(mysql) {
    super({ objectMode: true });

    this.mysql = mysql;
  }

  //$FlowFixMe
  _transform({ tenant, ts, t, v }, _, done) {
    this.mysql.execute(Writer.query, [tenant, ts, t, v], err => {
      if (!err) {
        this.push({ tenant, ts, t, v });
      }

      done(err);
    });
  }
}

module.exports = { Writer, WriterTransform };
