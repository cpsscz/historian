/* test suite for the staging package */

"use strict";

const test = require("ava");

const staging = require(".");

test("package export Historian symbol", t => t.truthy(staging.Historian));
