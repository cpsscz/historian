// @flow

"use strict";

const application = require("./application");

module.exports = { Application: application.Application };
