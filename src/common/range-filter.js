// @flow

"use strict";

const stream = require("stream");

class RangeFilter extends stream.Transform {
  static get range() {
    return {
      UPPER: 1,
      LOWER: 0
    };
  }

  constructor(range, limit) {
    super({ objectMode: true, allowHalfOpen: false });

    this.range = range;
    this.limit = limit;
  }

  _transform(chunk, _, done) {
    if (
      (this.range === RangeFilter.range.UPPER && chunk.t > this.limit) ||
      (this.range === RangeFilter.range.LOWER && chunk.t <= this.limit)
    ) {
      this.push(chunk);
    }

    done();
  }
}

module.exports = { RangeFilter };
