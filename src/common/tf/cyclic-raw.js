// @flow

"use strict";

const stream = require("stream");

/** Cyclic Raw Temporal Function
 *
 * This temporal function produce a result only after at least a given amount
 * of time (step) has passed. This is like the even temporal function but
 * with a global next time control.
 */
class CyclicRaw extends stream.Transform {
  constructor(ts, start, end, { step } = { step: 1000 }) {
    super({ objectMode: true, allowHalfOpen: false });

    this.step = step;
    this.nextT = start;
  }

  _transform({ t, v }, _, done) {
    if (t >= this.nextT) {
      this.push({ t, v });
      this.nextT = t + this.step;
    }

    done();
  }
}

module.exports = { CyclicRaw };
