// @flow

"use strict";

const stream = require("stream");

class Interception extends stream.Transform {
  constructor() {
    super({ objectMode: true, allowHalfOpen: false });
  }

  _transform({ t, v }, _, done) {
    if (v.filter(v => v !== null).length === v.length) {
      this.push({ t, v });
    }

    done();
  }
}

module.exports = { Interception };
