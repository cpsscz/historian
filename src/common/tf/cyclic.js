// @flow

"use strict";

const stream = require("stream");

class Cyclic extends stream.Transform {
  constructor(ts, start, end, { step } = { step: 1000 }) {
    super({ objectMode: true, allowHalfOpen: false });

    this.step = step;
    this.nextT = start;
    this.nextV = ts.map(() => null);
    this.end = end;
  }

  _transform({ t, v }, _, done) {
    while (t > this.nextT) {
      this.push({ t: this.nextT, v: this.nextV });
      this.nextT = this.nextT += this.step;
    }

    v.forEach((val, i) => {
      if (val !== null) {
        this.nextV[i] = val;
      }
    });

    if (t === this.nextT) {
      this.push({ t: this.nextT, v: this.nextV });
      this.nextT = this.nextT += this.step;
    }

    done();
  }

  _flush(done) {
    while (this.nextT < this.end) {
      this.push({ t: this.nextT, v: this.nextV });
      this.nextT = this.nextT += this.step;
    }

    done();
  }
}

module.exports = { Cyclic };
