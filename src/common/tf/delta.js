// @flow

"use strict";

const stream = require("stream");

/** Delta Temporal Function
 *
 * This temporal function produce a result where each time series output a
 * value only if that value is at least +/- step over the last value emitted.
 * Thus acting as a discretization function.
 */
class Delta extends stream.Transform {
  constructor(ts, start, end, { step } = { step: 1000 }) {
    super({ objectMode: true, allowHalfOpen: false });

    this.step = step;
    this.lower = ts.map(() => null);
    this.upper = ts.map(() => null);
  }

  _transform({ t, v }, _, done) {
    v.forEach((val, i) => {
      const numVal = val !== null ? JSON.parse(val) : null;

      // initialize nextStep, lower and upper if this is the first time we
      // se a value for his position
      if (val !== null && this.lower[i] === null && this.upper[i] === null) {
        this.lower[i] = numVal;
        this.upper[i] = numVal;
      }

      if (
        val !== null &&
        (numVal <= this.lower[i] || numVal >= this.upper[i])
      ) {
        this.upper[i] = numVal + this.step;
        this.lower[i] = numVal - this.step;
      } else {
        v[i] = null;
      }
    });

    if (v.filter(v => v !== null).length > 0) {
      this.push({ t, v });
    }

    done();
  }
}

module.exports = { Delta };
