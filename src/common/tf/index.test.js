/* test suite for the common/tf package */

"use strict";

const test = require("ava");

const tf = require(".");

test("package export interception symbol", t => t.truthy(tf.interception));

test("package export delta symbol", t => t.truthy(tf.delta));

test("package export even symbol", t => t.truthy(tf.even));
