// @flow

"use strict";

const stream = require("stream");

/** Even Temporal Function
 *
 * This temporal function produce a result where each time series output a
 * value only after at least a given amount of time (step) has passed after the
 * time of the last value emitted.
 */
class Even extends stream.Transform {
  constructor(ts, start, end, { step } = { step: 1000 }) {
    super({ objectMode: true, allowHalfOpen: false });

    this.step = step;
    this.nextStep = ts.map(() => start - step);
  }

  _transform({ t, v }, _, done) {
    v.forEach((val, i) => {
      if (val !== null && t >= this.nextStep[i]) {
        this.nextStep[i] = t + this.step;
      } else {
        v[i] = null;
      }
    });

    if (v.filter(v => v !== null).length > 0) {
      this.push({ t, v });
    }

    done();
  }
}

module.exports = { Even };
