"use strict";

const ava = require("ava");
const spec = require("stream-spec");
const tester = require("stream-tester");

const cyclic = require("./cyclic");

let stream;

ava.beforeEach(() => {
  stream = new cyclic.Cyclic([], Date.now() - 10000, Date.now(), {
    step: 1
  });

  spec(stream)
    .through({ strict: false })
    .validateOnExit();
});

ava.skip.cb("should pass records down the line", t => {
  const pause = tester
    .createRandomStream(function() {
      return { t: Date.now(), v: [] };
    }, 1000)
    .pipe(stream)
    .pipe(tester.createPauseStream());

  pause.on("end", t.end);
  pause.on("error", t.end);
});
