// @flow

"use strict";

const stream = require("stream");

const multistream = require("multistream");
const pump = require("pump");

/*::
import { Historian } from "../historian";
*/

const batchDelete = require("./batch-delete");
const measureSpeed = require("./measure-speed");
const rangeFilter = require("./range-filter");
const stream2multiple = require("./stream2multiple");
const tsCollectionStreamCombine = require("./ts-collection-stream-combine");

class MergeHistorians /*:: implements Historian */ {
  /*::
  staging: Historian;
  final: Historian;
  limit: number;
  */

  constructor(
    staging /*: Historian */,
    final /*: Historian */,
    limit /*: number */ = 1000 * 60 * 60 * 24
  ) {
    this.staging = staging;
    this.final = final;
    this.limit = limit;
  }

  /** Repair Time Series
   *
   * Moves data from the staging historian to the final historian according to
   * the limit established for this merge.
   *
   * @param {String} tenant Tenant
   * @param {String} ts Time Series unique identifier
   */
  async repair(tenant /*: string */, ts /*: string */) {
    let start = 0;
    let end = Date.now() - this.limit;

    const source = new measureSpeed.MeasureSpeedTransform();

    pump(await this.staging.query(tenant, ts, start, end), source);

    const destination = new batchDelete.BatchDelete();

    pump(
      destination,
      new stream.Writable({
        objectMode: true,
        write: ({ tenant, ts, t }, _, done) => {
          console.log("deleting from staging", tenant, ts, t);
          this.staging
            .delete(tenant, ts, t[0], t[t.length - 1])
            .then(done, done);
        }
      })
    );

    await this.final.insert(tenant, ts, source, destination);
  }

  async list(tenant /*: string */) {
    const listStreams = await Promise.all([
      this.staging.list(tenant),
      this.final.list(tenant)
    ]);

    return pump(
      new tsCollectionStreamCombine.TSCollectionStreamCombine(listStreams),
      new stream.Transform({
        objectMode: true,
        allowHalfOpen: false,
        transform: ({ ts }, _, done) => done(null, { ts })
      })
    );
  }

  async query(
    tenant /*: string */,
    ts /*: string */,
    start /*: number */,
    end /*: number */
  ) {
    const limit = Date.now() - this.limit;

    // if all data is in the staging area
    if (start >= limit) {
      return await this.staging.query(tenant, ts, start, end);
    } else if (end <= limit) {
      return await this.final.query(tenant, ts, start, end);
    } else {
      // if there is data in both historians
      let count = 0;
      return multistream(
        done => {
          console.log(
            "switching source stream",
            tenant,
            ts,
            start,
            end,
            end > limit ? limit - 1 : end,
            count
          );
          count += 1;

          switch (count) {
            case 1:
              this.final
                .query(tenant, ts, start, end > limit ? limit - 1 : end)
                .then(stream => done(null, stream), err => done(err));
              break;
            case 2:
              this.staging
                .query(tenant, ts, start, end)
                .then(stream => done(null, stream), err => done(err));
              break;
            default:
              done(null, null);
              break;
          }
        },
        { objectMode: true }
      );
    }
  }

  insert(
    tenant /*: string */,
    ts /*: string */,
    readable /*: stream.Readable */,
    writable
  ) {
    const limit = Date.now() - this.limit;

    const staging = new rangeFilter.RangeFilter(
      rangeFilter.RangeFilter.range.UPPER,
      limit
    );

    this.staging.insert(tenant, ts, staging);

    const final = new rangeFilter.RangeFilter(
      rangeFilter.RangeFilter.range.LOWER,
      limit
    );

    this.final.insert(tenant, ts, final);

    const destinations = [final, staging];

    if (writable) {
      destinations.push(writable);
    }

    return new Promise((resolve, reject) => {
      pump(
        readable,
        new stream2multiple(destinations, { objectMode: true }),
        err => (err ? reject(err) : resolve())
      );
    });
  }
}

module.exports = { MergeHistorians };
