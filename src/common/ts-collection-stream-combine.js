// @flow

"use strict";

const stream = require("stream");

class TSCollectionStreamCombine extends stream.Readable {
  constructor(streams) {
    super({
      objectMode: true
    });

    this.streams = streams;
    this.key = "ts";

    this.ended = this.streams.map(() => false);
    this.current = this.streams.map(() => null);
    this.indexes = this.streams.map((_, i) => i);

    this.busy = false;

    this.streams.forEach((stream, index) => {
      stream.on("error", function(error) {
        return this.emit("error", error);
      });
      stream.on("end", () => this.handleEnd(index));
      stream.on("data", data => this.handleData(index, data));
    });
  }

  _read() {
    if (this.busy) {
      return;
    }
    this.busy = true;
    return this.resumeStreams();
  }

  getLowestKeyIndexes() {
    let i, index, keys, len, object, ref, skip;
    keys = [];
    skip = false;
    ref = this.current;
    for (index = i = 0, len = ref.length; i < len; index = ++i) {
      object = ref[index];
      if (object) {
        keys[index] = object[this.key];
      } else {
        if (this.ended[index]) {
          // keys[index] = Infinity;
        } else {
          skip = true;
          break;
        }
      }
    }
    if (skip) {
      return [];
    }
    this.lowest = keys.sort().shift();

    return this.current
      .map((object, index) => {
        if (object && object[this.key] === this.lowest) {
          return index;
        }
      })
      .filter(index => index !== null && index !== undefined);
  }

  resumeStreams() {
    let i, index, len, reEvaluatePush, ref;
    reEvaluatePush = false;
    ref = this.indexes;
    for (i = 0, len = ref.length; i < len; i++) {
      index = ref[i];
      this.current[index] = null;
      if (this.ended[index]) {
        if (!reEvaluatePush) {
          reEvaluatePush = true;
        }
      } else {
        this.streams[index].resume();
      }
    }
    if (reEvaluatePush) {
      return this.evaluatePush();
    }
  }

  evaluatePush() {
    let pushMore; //, send;
    const indexes = this.getLowestKeyIndexes();

    if (!indexes.length) {
      return;
    }
    // send = {
    //   data: indexes.map(index => this.current[index]),
    //   indexes: indexes
    // };
    // send[this.key] = this.lowest;
    pushMore = this.push({ ts: this.lowest });
    if (!pushMore) {
      this.busy = false;
      return;
    }
    return this.resumeStreams();
  }

  handleData(index, object) {
    this.streams[index].pause();
    this.current[index] = object;
    return this.evaluatePush();
  }

  handleEnd(index) {
    this.ended[index] = true;
    this.evaluatePush();
    if (this.ended.every(x => x)) {
      return this.push(null);
    }
  }
}

module.exports = { TSCollectionStreamCombine };
