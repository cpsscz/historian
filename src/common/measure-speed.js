// @flow

"use strict";

const stream = require("stream");

class MeasureSpeedTransform extends stream.Transform {
  /*::
  records: number;
  cycle: number;
  minSpeed: number;
  maxSpeed: number;
  meanSpeed: number;
  total: number;
  timer: *;
  */

  constructor() {
    super({ objectMode: true, allowHalfOpen: false });

    this.records = 0;
    this.cycle = 1;
    this.minSpeed = Infinity;
    this.maxSpeed = 0;
    this.meanSpeed = 0;
    this.total = 0;
  }

  reportSpeed() {
    const current = this.records / 60;

    this.total += this.records;

    if (current < this.minSpeed) {
      this.minSpeed = current;
    }

    if (current > this.maxSpeed) {
      this.maxSpeed = current;
    }

    this.meanSpeed = this.meanSpeed + (current - this.meanSpeed) / this.cycle;

    console.log(
      "Transfer speed",
      "current:",
      current.toFixed(2),
      "r/s",
      "min:",
      this.minSpeed.toFixed(2),
      "r/s",
      "mean:",
      this.meanSpeed.toFixed(2),
      "r/s",
      "max:",
      this.maxSpeed.toFixed(2),
      "r/s",
      "at cycle",
      this.cycle,
      "total records",
      this.total
    );

    this.records = 0;
    this.cycle += 1;
  }

  _transform(chunk /*: any */, _ /*: any */, done /*: Function */) {
    if (!this.timer) {
      this.timer = setInterval(() => this.reportSpeed(), 1000 * 60);
    }

    this.records += 1;

    done(null, chunk);
  }

  _flush(done /*: Function */) {
    clearInterval(this.timer);
    this.reportSpeed();
    done();
  }
}

module.exports = { MeasureSpeedTransform };
