// @flow

"use strict";

const stream = require("stream");

/** Set Time Series transformation stream
 *
 * Set the `ts` dict item on each transformed record from fixed value pass to
 * the constructor.
 */
class SetTenantTSTransform extends stream.Transform {
  /*::
  ts: string;
  */

  /** Constructor
   *
   * @param {String} ts Time Series unique identifier
   */
  constructor(tenant /*: string */, ts /*: string */) {
    super({ objectMode: true, allowHalfOpen: false });

    this.tenant = tenant;
    this.ts = ts;
  }

  //$FlowFixMe
  _transform(chunk, _, done) {
    Object.assign(chunk, { tenant: this.tenant, ts: this.ts });

    done(null, chunk);
  }
}

module.exports = { SetTenantTSTransform };
