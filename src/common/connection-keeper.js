// @flow

"use strict";

const stream = require("stream");

class ConnectionKeeper extends stream.Transform {
  constructor() {
    super({ objectMode: true });

    // set initial timer
    this.resetTimer();
  }

  resetTimer() {
    if (this.timer) {
      clearTimeout(this.timer);
    }

    this.timer = setTimeout(() => this.onTimeout(), 1000);
  }

  onTimeout() {
    this.push({ "connection-keep": true });

    this.timer = undefined;

    this.resetTimer();
  }

  _transform(chunk, _, done) {
    this.resetTimer();

    done(null, chunk);
  }

  _flush(done) {
    if (this.timer) {
      clearTimeout(this.timer);
    }

    done();
  }
}

module.exports = { ConnectionKeeper };
