// @flow

"use strict";

const stream = require("stream");

class LimitSteam extends stream.Transform {
  /** Constructor
   *
   * Source stream is needed in order to be able to unpipe it from this
   * transform before ending this and avoid any error cause by the source
   * trying to write to a closed stream.
   */
  constructor(source, limit = 10000) {
    super({ objectMode: true, allowHalfOpen: false });

    this.source = source;
    this.count = 0;
    this.limit = limit;
  }

  _transform(chunk, _, done) {
    if (this.count < this.limit) {
      this.push(chunk);
    }

    this.count += 1;

    if (this.count === this.limit) {
      this.push({ "limit-reach": true });

      // prevent error if source try to write to this stream after EOF
      this.source.unpipe(this);

      this.push(null);
    }

    done();
  }
}

module.exports = { LimitSteam };
