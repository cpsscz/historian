// @flow

"use strict";

const stream = require("stream");

class BatchDelete extends stream.Transform {
  constructor() {
    super({ objectMode: true, allowHalfOpen: false });

    this.size = 100;
    this.buffer = [];
  }

  _transform({ tenant, ts, t }, _, done) {
    this.buffer.push({ tenant, ts, t });

    if (this.buffer.length === this.size) {
      this.push({ tenant, ts, t: this.buffer.map(i => i.t) });
      this.buffer = [];
    }

    done();
  }

  _flush(done) {
    if (this.buffer.length > 0) {
      this.push({
        tenant: this.buffer[0].tenant,
        ts: this.buffer[0].ts,
        t: this.buffer.map(i => i.t)
      });
    }
    done();
  }
}

module.exports = { BatchDelete };
