// @flow

"use strict";

const historian = require("./historian");

module.exports = { Historian: historian.Historian };
