// @flow

"use strict";

const stream = require("stream");

const uuidv3 = require("uuid/v3");

/*::
const mysql = require("mysql2");
*/

/** Stream transform that provide location information to records.
 *
 *
 */
class LocationProvider extends stream.Transform {
  static get selectLastQuery() {
    return (
      "SELECT * FROM `locations` WHERE `tenant` = ? AND `ts` = ? " +
      "AND `records` < ?"
    );
  }

  static get increaseRecordsQuery() {
    return (
      "UPDATE `locations` SET `records` = `records` + ?, `end` = ? " +
      "WHERE `tenant` = ? AND `ts` = ? AND `partition` = ? AND `records` < ?"
    );
  }

  static get createLocationQuery() {
    return (
      "INSERT INTO `locations` " +
      "(`tenant`, `ts`, `htable`, `partition`, `start`) " +
      "VALUES (?, ?, ?, ?, ?)"
    );
  }

  static get closeLocationQuery() {
    return (
      "UPDATE `locations` SET `end` = ?, `records` = ? " +
      "WHERE `tenant` = ? AND `ts` = ? AND `partition` = ?"
    );
  }

  /*::
  mysql: mysql.Client;
  records: number;
  ts: string;
  partitionSize: number;
  lastT: number
  */

  constructor(
    mysql /*: mysql.Client */,
    tenant /*: string */,
    ts /*: string */,
    partitionSize /*: number */ = 1000000
  ) {
    super({ objectMode: true, allowHalfOpen: false });

    this.mysql = mysql;

    this.records = 0;
    this.tenant = tenant;
    this.ts = ts;
    this.partitionSize = partitionSize;
    this.location = undefined;
    this.lastT = 0;
  }

  //$FlowFixMe
  async _transform(chunk, _, done) {
    // retrieve partition information if not retrieved before
    if (!this.location) {
      const [locations] = await this.mysql.execute(
        LocationProvider.selectLastQuery,
        [this.tenant, this.ts, this.partitionSize]
      );

      this.location = locations ? locations[0] : undefined;

      if (this.location) {
        this.records = this.location.records;

        console.log(
          "retrieved location data",
          this.location.htable,
          this.location.partition,
          this.location.records
        );
      }
    }

    // if no location found create one
    if (!this.location) {
      this.location = {
        tenant: this.tenant,
        ts: this.ts,
        htable: "records",
        partition: uuidv3(Date.now().toString(), this.ts).toString(),
        records: 0
      };

      await this.mysql.execute(LocationProvider.createLocationQuery, [
        this.location.tenant,
        this.location.ts,
        this.location.htable,
        this.location.partition,
        chunk.t
      ]);

      console.log(
        "created location",
        this.location.htable,
        this.location.partition,
        this.location.records
      );
    }

    const result = Object.assign({}, chunk, {
      table: this.location.htable,
      p: this.location.partition
    });

    this.push(result);

    this.records += 1;
    this.lastT = result.t;

    // close partition if filled
    if (this.records >= this.partitionSize) {
      console.log(
        "closing location",
        this.records,
        this.location.htable,
        this.location.partition,
        this.location.records
      );
      await this.mysql.execute(LocationProvider.closeLocationQuery, [
        chunk.t,
        this.records,
        this.location.tenant,
        this.location.ts,
        this.location.partition
      ]);

      this.records = 0;
      delete this.location;
    }

    done();
  }

  //$FlowFixMe
  async _flush(done) {
    if (this.records > 0) {
      console.log(
        "flushing location",
        this.records,
        this.location.htable,
        this.location.partition,
        this.location.records
      );
      await this.mysql.execute(LocationProvider.increaseRecordsQuery, [
        this.records - this.location.records,
        this.lastT,
        this.location.tenant,
        this.location.ts,
        this.location.partition,
        this.partitionSize
      ]);
    }

    done();
  }
}

/** Time Series record locator.
 *
 * This class implement the record location strategy in use by the Process
 * Historian. It control the placement of each record of data inside the
 * Cassandra Database in order to achieve the best performance for reads and
 * writes.
 */
class RecordLocator {
  static get select() {
    return (
      "SELECT * FROM `locations`\n" +
      "    WHERE `tenant` = ? AND `ts` = ?\n" +
      "    AND ((start >= ? AND end <= ?)\n" + // this part handle the case of time range inside a single partition
      "    OR (start <= ? AND end <= ?)\n" +
      "    OR (start >= ? AND end >= ?)\n" +
      "    OR (start <= ? AND end >= ?))"
    );
  }

  /*::
  mysql: mysql.Client;
  partitionSize: number;
  */

  constructor(
    mysql /*: mysql.Client */,
    partitionSize /*: number */ = 1000000
  ) {
    this.mysql = mysql;

    this.partitionSize = partitionSize;
  }

  /** Provide location information to a stream of Time Series records
   *
   * This method return a stream transformation to provide location
   * characteristics to individual records. It is an essential part of the
   * process of ingesting data into the historian.
   *
   * @param {String} tenant Tenant
   * @param {String} ts Time Series unique identifier
   * @return {Promise<stream.Transform<Record>>}
   */
  provideLocation(tenant /*: string */, ts /*: string */) {
    return new LocationProvider(this.mysql, tenant, ts, this.partitionSize);
  }

  async getLastLocation(tenant /*: string */, ts /*: string */) {
    const [locations] = await this.mysql.execute(
      LocationProvider.selectLastQuery,
      [tenant, ts, this.partitionSize]
    );

    return locations && locations.length > 0 ? locations[0] : null;
  }

  async getLocations(
    tenant /*: string */,
    ts /*: string */,
    start /*: number */,
    end /*: number */
  ) {
    const [locations] = await this.mysql.execute(RecordLocator.select, [
      tenant,
      ts,
      start,
      end,
      start,
      end,
      start,
      end,
      start,
      end
    ]);

    return locations.map(location => ({
      table: location.htable,
      p: location.partition,
      t: [
        location.start < start ? start : location.start,
        !location.end || location.end > end ? end : location.end
      ]
    }));
  }
}

module.exports = { RecordLocator };
