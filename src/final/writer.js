// @flow

"use strict";

const stream = require("stream");

/*::
const cassandraDriver = require("cassandra-driver");
*/

class Writer extends stream.Writable {
  static insert(table /*: string */) {
    return `INSERT INTO ${table} (p, t, v) VALUES (?, ?, ?)`;
  }

  /*::
  cassandra: cassandraDriver.Client;
  */

  constructor(cassandra /*: cassandraDriver.Client */) {
    super({ objectMode: true, highWaterMark: 1 });

    this.cassandra = cassandra;
  }

  //$FlowFixMe
  _write(batch, _, done) {
    this.cassandra.batch(
      batch.map(({ table, p, t, v }) => ({
        query: Writer.insert(table),
        params: [p, t, v]
      })),
      {
        hints: batch.map(() => ["uuid", "bigint", "text"]),
        prepare: true,
        logged: false
      },
      done
    );
  }
}

class WriterTransform extends stream.Transform {
  /*::
  cassandra: cassandraDriver.Client;
  */

  constructor(cassandra /*: cassandraDriver.Client */) {
    super({ objectMode: true, highWaterMark: 1 });

    this.cassandra = cassandra;
  }

  //$FlowFixMe
  _transform(batch, _, done) {
    this.cassandra.batch(
      batch.map(({ table, p, t, v }) => ({
        query: Writer.insert(table),
        params: [p, t, v]
      })),
      {
        hints: batch.map(() => ["uuid", "bigint", "text"]),
        prepare: true,
        logged: false
      },
      err => {
        if (!err) {
          batch.forEach(r => this.push(r));
        }

        done(err);
      }
    );
  }
}

module.exports = { Writer, WriterTransform };
