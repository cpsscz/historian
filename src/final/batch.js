// @flow

"use strict";

const stream = require("stream");

/*::
import { BatchRecord } from "./batch-record";
*/

class BatchTransform extends stream.Transform {
  /*::
  size: number;
  batch: BatchRecord[];
  */

  constructor(size /*: number */ = 1000) {
    super({ objectMode: true, allowHalfOpen: false });

    this.size = size;
    this.batch = [];
  }

  //$FlowFixMe
  _transform(r, _, done) {
    // console.log("BatchTransform._transform", r);

    // check record belongs to the same batch otherwise flush batch
    if (
      this.batch.length > 0 &&
      (this.batch[0].table !== r.table || this.batch[0].p !== r.p)
    ) {
      this._flush(() => null);
    }

    this.batch.push(r);

    if (this.batch.length === this.size) {
      this._flush(() => null);
    }

    done();
  }

  _flush(done) {
    if (this.batch.length > 0) {
      // console.log("BatchTransform._flush", this.batch.length);
      this.push(this.batch);
      this.batch = [];
    }

    done();
  }
}

module.exports = { BatchTransform };
