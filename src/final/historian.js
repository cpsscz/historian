// @flow

"use strict";

const stream = require("stream");

const cassandraDriver = require("cassandra-driver");
const multistream = require("multistream");
const mysql = require("mysql2");
const pump = require("pump");

/*::
import { Historian as HistorianInterface } from "../historian";
*/

const setTenantTs = require("../common/set-tenant-ts");

const batch = require("./batch");
const recordLocator = require("./record-locator");
const writer = require("./writer");

class Historian /*:: implements HistorianInterface */ {
  static get DEFAULT_SETTINGS() {
    return {
      DATABASE_URL: "mysql://user:password@mysql/db",
      CASSANDRA_SETTINGS: JSON.stringify({
        contactPoints: ["node"],
        keyspace: "historian"
      }),
      batchSize: "10000",
      partitionSize: "1000000"
    };
  }

  /*::
  settings: any;

  _mysql: mysql.Client;
  _cassandra: cassandraDriver.Client;
  _recordLocator: recordLocator.RecordLocator;
  */

  constructor(settings /*: any */ = {}) {
    this.settings = Object.assign({}, Historian.DEFAULT_SETTINGS, settings);
  }

  get mysql() {
    if (!this._mysql) {
      this._mysql = mysql.createPool(this.settings.DATABASE_URL);
    }

    return this._mysql;
  }

  get cassandra() {
    if (!this._cassandra) {
      this._cassandra = new cassandraDriver.Client(
        JSON.parse(this.settings.CASSANDRA_SETTINGS)
      );

      // this._cassandra.on("log", function(level, className, message) {
      //   console.log("log event: %s -- %s", level, message);
      // });
    }

    return this._cassandra;
  }

  get recordLocator() {
    if (!this._recordLocator) {
      this._recordLocator = new recordLocator.RecordLocator(
        this.mysql.promise(),
        parseInt(this.settings.partitionSize, 10)
      );
    }

    return this._recordLocator;
  }

  async list(tenant /*: string */) {
    return this.mysql
      .execute(
        "SELECT DISTINCT `ts` FROM `locations` WHERE `tenant` = ? " +
          "ORDER BY `ts` ASC",
        [tenant]
      )
      .stream();
  }

  async query(
    tenant /*: string */,
    ts /*: string */,
    start /*: number */,
    end /*: number */
  ) {
    if (end === Infinity) {
      end = Math.pow(2, 64) - 1;
    }

    const locations = await this.recordLocator.getLocations(
      tenant,
      ts,
      start,
      end
    );

    return pump(
      multistream(
        locations.map(({ table, p }) => () => {
          return this.cassandra.stream(
            `SELECT t, v FROM ${table} WHERE p = ? AND t >= ? AND t <= ?`,
            [p, start, end],
            {
              hints: ["uuid", "bigint", "bigint"]
            }
          );
        }),
        { objectMode: true }
      ),
      new stream.Transform({
        objectMode: true,
        allowHalfOpen: false,
        transform: (chunk, _, done) =>
          done(null, Object.assign(chunk, { t: parseInt(chunk.t, 10) }))
      })
    );
  }

  insert(
    tenant /*: string */,
    ts /*: string */,
    readable /*: stream.Readable */,
    writable
  ) {
    return new Promise((resolve, reject) => {
      const pipeline = [
        readable,
        new setTenantTs.SetTenantTSTransform(tenant, ts),
        this.recordLocator.provideLocation(tenant, ts),
        new batch.BatchTransform(parseInt(this.settings.batchSize, 10))
      ];

      if (writable) {
        pipeline.push(new writer.WriterTransform(this.cassandra));
        pipeline.push(writable);
      } else {
        pipeline.push(new writer.Writer(this.cassandra));
      }

      pump(...pipeline, err => (err ? reject(err) : resolve()));
    });
  }
}

module.exports = { Historian };
