// @flow

"use strict";

/*::
const stream = require("stream");
*/

const cors = require("cors");
const express = require("express");
const morgan = require("morgan");
const statsd = require("express-statsd");

const final = require("./final");
const mergeHistorian = require("./common/merge-historians");
const staging = require("./staging");
const web = require("./web");

/** REST API Application
 *
 * This class encapsulates the entire REST API logic into a single reusable
 * component.
 */
class Application extends mergeHistorian.MergeHistorians {
  /*::
  settings: any;

  insert: (string, stream.Readable) => Promise<void>;

  // web request event listener cached for better performance
  _listener: express;
  */

  static get DEFAULT_SETTINGS() {
    return {
      listener: JSON.stringify({
        "case sensitive routing": true,
        etag: false,
        "strict routing": true,
        "x-powered-by": false,
        "trust proxy": true
      })
    };
  }

  constructor(settings /*: any */) {
    settings = Object.assign({}, Application.DEFAULT_SETTINGS, settings);

    super(
      new staging.Historian({
        DATABASE_URL: settings.STAGING_DATABASE_URL
      }),
      new final.Historian({
        DATABASE_URL: settings.FINAL_DATABASE_URL,
        CASSANDRA_SETTINGS: settings.CASSANDRA_SETTINGS
      })
    );

    this.settings = settings;
  }

  /** Web `request` event listener
   *
   * This event listener assemble the logic required to handle all Web
   * `request` events directed to the REST API.
   */
  get listener() {
    if (!this._listener) {
      this._listener = express();

      // configure express
      const settings = JSON.parse(this.settings.listener);
      Object.keys(settings).forEach(key =>
        this.listener.set(key, settings[key])
      );

      // setup request logging
      this._listener.use(morgan("combined"));

      // enable express statsd instrumentation if settings provided
      if (this.settings.statsd) {
        this._listener.use(statsd(JSON.parse(this.settings.statsd)));
      }

      // setup CORS support
      this._listener.options(
        "*",
        cors({
          exposedHeaders: ["Location"]
        })
      );
      this._listener.use(
        cors({
          exposedHeaders: ["Location"]
        })
      );

      // configure web request router
      this._listener.use(web.setupRouter(express.Router(), this));
    }

    return this._listener;
  }
}

module.exports = { Application };
