# Sorbotics Process Historian

Este projecto implementa el Historian de Procesos de Sorbotics. 

## Documentacion

  - [Conceptos](./doc/concepts.md)
  - [API REST](./doc/rest-api.md)
  - [Actualizacion del Esquema de Cassandra](./doc/cassandra-schema-update.md)